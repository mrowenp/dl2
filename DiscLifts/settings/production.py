from .base import *

# Checks whether environment variables have been loaded properly , if not, ImproperlyConfigured error raised.
def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_env_variable('DATABASE_NAME'),
        'USER': get_env_variable('DATABASE_USER'),
        'PASSWORD': get_env_variable('DATABASE_PASSWORD'),
        'HOST': get_env_variable('DATABASE_HOST'),
        'PORT': get_env_variable('DATABASE_PORT'),
    }
}

DEBUG = False
ALLOWED_HOSTS = ['178.62.13.161', 'disclifts.co.uk', 'www.disclifts.co.uk']

STATIC_URL = '/collectedstatic/'

# The absolute path to the directory where collectstatic will collect static files for deployment.
# Running collectstatic collects all the static from STATICFILES_DIRS and dumps it to the the static_root dir ready for
# deployment
STATIC_ROOT =  os.path.join(BASE_DIR, 'collectedstatic/')

